package Lab3

import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

import scala.util.Random

class KnownGamesTest extends AnyWordSpec with Matchers {

  "A Family" should {
    val matr = Array(Array((4, 1), (0, 0)), Array((0, 0), (1, 4)))
    "be the same on Nash" in {
      Task1.getNash(matr) should contain theSameElementsAs Array((4, 1), (1, 4))
    }
    "be the same on Pareto" in {
      Task1.getPareto(matr) should contain theSameElementsAs Array((4, 1), (1, 4))
    }
  }

  "Prisoners" should {
    val matr = Array(Array((-5, -5), (0, -10)), Array((-10, 0), (-1, -1)))
    "be the same on Nash" in {
      Task1.getNash(matr) should contain theSameElementsAs Array((-5, -5))
    }
    "be the same on Pareto" in {
      Task1.getPareto(matr) should contain theSameElementsAs Array((-1, -1), (-10, 0), (0, -10))
    }
  }

  "Road" should {
    val e1 = Random.between(0.0, 1.0)
    val e2 = Random.between(0.0, 1.0)
    val matr = Array(Array((1.0, 1.0), (1.0 - e1, 2.0)), Array((2.0, 1.0 - e2), (0.0, 0.0)))
    "be the same on Nash" in {
      Task1.getNash(matr) should contain theSameElementsAs Array((1 - e1, 2), (2, 1 - e2))
    }
    "be the same on Pareto" in {
      Task1.getPareto(matr) should contain theSameElementsAs Array((1 - e1, 2), (2, 1 - e2), (1, 1))
    }
  }

}
