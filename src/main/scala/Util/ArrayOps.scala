package Util

object ArrayOps {
  implicit class ArrayOps[T](self: Array[T]) {
    def printMe(): Unit = {
      self.foreach {
        case arr: Array[_] => arr.printMe()
        case a: Double => print(f" $a%5.2f ")
        case a => print(a.toString + " ")
      }
      println()
    }
  }
}
