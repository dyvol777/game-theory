package RK1

case class MySphere(R: Double, k: Int, locality: Double) {
  val g = 1.32471795724474602596
  val a1: Double = 1.0 / g
  val a2: Double = 1.0 / (g * g)
  val points: List[Point] = List.tabulate(k)(n =>
    Point(
      (0.5 + a1 * (n + 1)) % 1 * math.Pi,
      (0.5 + a2 * (n + 1)) % 1 * 2 * math.Pi,
      R)
  )

  //  println("Areas:")
  //  points.foreach { p1 =>
  //    points.foreach(p2 => print(p1.distance(p2) + " "))
  //    println()
  //  }

  def check(p: Point): Boolean =
    points.exists(_.distance(p) <= locality)


  def getAnalytic: Double = {
    val sphereS = 4 * math.Pi * R * R

    val P = (2 * R + locality) / 2
    val triangleS = math.sqrt(P * (P - R) * (P - R) * (P - locality))
    val H = 2 * triangleS / R
    val h = math.sqrt(locality * locality - H * H)

    val smallS = 2 * math.Pi * R * h

    smallS * k / sphereS
  }
}
