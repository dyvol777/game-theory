package RK1

import scala.math.{cos, sin, sqrt}
import scala.util.Random

case class Point(angle1: Double, angle2: Double, R: Double) {
  lazy val x: Double = R * sin(angle1) * cos(angle2)
  lazy val y: Double = R * sin(angle1) * sin(angle2)
  lazy val z: Double = R * cos(angle1)

  def distance(p: Point): Double =
    sqrt((x - p.x) * (x - p.x) + (y - p.y) * (y - p.y) + (z - p.z) * (z - p.z))
}

object Point {
  def RandomPoint(r: Double): Point =
    Point(Random.nextDouble(), Random.nextDouble(), r)
}
