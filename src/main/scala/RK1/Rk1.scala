package RK1

import scalafx.Includes._
import scalafx.application.JFXApp
import scalafx.application.JFXApp.PrimaryStage
import scalafx.beans.property.DoubleProperty
import scalafx.scene._
import scalafx.scene.input.MouseEvent
import scalafx.scene.paint.{Color, PhongMaterial}
import scalafx.scene.shape.Sphere
import scalafx.scene.transform.Rotate

import scala.collection.parallel.CollectionConverters._

object Rk1 extends JFXApp {
  val r = 200 //abs(Random.nextInt() % 100)
  val k = 1000 //abs(Random.nextInt % 5) + 1
  val l: Double = 5 //r / abs(Random.nextInt() % 10 + 1)
  val iterates = 1000000

  println(s"Радиус = $r; количество точек = $k; окрестность точки = $l; количество игр = $iterates")

  val s: MySphere = MySphere(r, k, l)
  println(s"Аналитическая цена игры: ${s.getAnalytic}")

  var n = 0
  (1 to iterates).par.foreach { _ =>
    val p = Point.RandomPoint(r)
    if (s.check(p)) {
      n += 1
    }
  }
  print(s"Практическая цена игры: ${n.toDouble / iterates}")

  stage = new PrimaryStage {
    title = "Sphere Game"
    scene = new Scene(500, 500, true, SceneAntialiasing.Balanced) {

      val sphere: Sphere = new Sphere(r) {
        material = new PhongMaterial {
          diffuseColor = Color.Blue
          specularColor = Color.LightBlue
        }
      }

      val k: Int = 5000 //abs(Random.nextInt % 5) + 1
      val points1: List[Sphere] = s.points.map(
        p => new Sphere(l) {
          translateX = p.x
          translateY = p.y
          translateZ = p.z
        }
      )

      // Put shapes in a groups so they can be rotated together
      val shapes1 = new Group(points1: _*)
      val shapes = new Group(shapes1, sphere)


      val light: PointLight = new PointLight {
        color = Color.AntiqueWhite
        translateX = -265
        translateY = -260
        translateZ = -625
      }

      root = new Group {
        // Put light outside of `shapes` group so it does not rotate
        children = new Group(shapes, light)
        translateX = 250
        translateY = 250
        translateZ = 725
        rotationAxis = Rotate.YAxis
      }

      camera = new PerspectiveCamera(false)

      addMouseInteraction(this, shapes)
    }
  }

  /** Add mouse interaction to a scene, rotating given node. */
  private def addMouseInteraction(scene: Scene, node: Node): Unit = {
    val angleY = DoubleProperty(-50)
    val angleX = DoubleProperty(-50)
    val yRotate = new Rotate {
      angle <== angleY
      axis = Rotate.YAxis
    }
    val xRotate = new Rotate {
      angle <== angleX
      axis = Rotate.XAxis
    }

    var anchorX: Double = 0
    var anchorY: Double = 0
    var anchorAngleY: Double = 0
    var anchorAngleX: Double = 0

    node.transforms = Seq(yRotate, xRotate)

    scene.onMousePressed = (event: MouseEvent) => {
      anchorX = event.sceneX
      anchorY = event.sceneY
      anchorAngleY = angleY()
      anchorAngleX = angleX()
    }
    scene.onMouseDragged = (event: MouseEvent) => {
      angleY() = anchorAngleY + anchorX - event.sceneX
      angleX() = anchorAngleX - (anchorY - event.getSceneY)
    }
  }
}
