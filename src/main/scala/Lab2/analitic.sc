val a = -4
val b = 4
val c = 8
val d = -12 / 5
val e = -28 / 5

def f(x: Double, y: Double): Double = {
  a * x * x + b * y * y + c * x * y + d * x + e * y
}

f(0.2, 0.5)

math.abs(-1.2816 - f(0.2, 0.5))