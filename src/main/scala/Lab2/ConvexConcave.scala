package Lab2

import Lab1.AlgBraunaRobinson
import Util.ArrayOps.ArrayOps

case class ConvexConcave(a: Double,
                         b: Double,
                         c: Double,
                         d: Double,
                         e: Double) {
  def work(): Unit = {
    val e = 0.001
    val n = 5
    lazy val lazyList: LazyList[Double] = LazyList.iterate(2)(_ + 1).map(x => getPrice(genMatrix(x)))

    lazy val q = lazyList.zipWithIndex.takeWhile(
      x => lazyList.slice(x._2 + 1, x._2 + n + 1).exists(
        y => {
          math.abs(x._1 - y) > e
        }
      )
    )

    print("Price = " + lazyList(q.last._2 + 1))
    println()
  }

  private def genMatrix(n: Int) =
    Array.tabulate(n + 1)(
      i => Array.tabulate(n + 1)(j => f(i.toDouble / n, j.toDouble / n))
    )

  private def f(x: Double, y: Double): Double = {
    a * x * x + b * y * y + c * x * y + d * x + e * y
  }

  private def getPrice(matr: Array[Array[Double]]) = {
    println("Dimension = " + (matr.length - 1))
    matr.printMe()
    val k = check(matr)
    val n = matr.length - 1
    k match {
      case Some(k) =>
        println(s"Седловая точка: ${matr(k).min} x= ${k.toDouble / n}, y=${matr(k).zipWithIndex.minBy(_._1)._2.toDouble / n}")
        println()
        matr(k).min
      case None =>
        val (strategyA, strategyB) = AlgBraunaRobinson(matr).start(true)

        //        println("strategy A:")
        //        strategyA.printMe()
        //
        //        println("strategy B:")
        //        strategyB.printMe()

        val price = matr.zipWithIndex.map(
          x => x._1.zipWithIndex.map(
            y => y._1 * strategyB(y._2)
          ).sum * strategyA(x._2)
        ).sum
        println(s"Price = $price")

        val ind = matr.flatten.map(x => math.abs(x - price)).zipWithIndex.minBy(_._1)._2
        val x = ind / (n + 1)
        val y = ind % (n + 1)

        println(s"True price: ${matr(x)(y)}, x = ${x.toDouble / n}, y = ${y.toDouble / n}")
        println()
        matr(x)(y)
    }
  }

  private def check(matr: Array[Array[Double]]): Option[Int] = {
    val trans = matr.transpose
    for (k <- matr.indices)
      if (matr(k).min == trans(matr(k).zipWithIndex.minBy(_._1)._2).max)
        return Some(k)
    None
  }

}
