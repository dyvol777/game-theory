package Lab3

import Util.ArrayOps.ArrayOps

import scala.util.Random

object Task1 extends App {

  def NashOptimal[A](matr: Array[Array[(A, A)]], x: Int, y: Int)(implicit n: Numeric[A]): Boolean = {
    import n._
    if (
      matr(x).exists(_._2 > matr(x)(y)._2) ||
        matr.transpose.apply(y).exists(_._1 > matr(x)(y)._1)
    ) return false
    true
  }

  def getNash[A](matr: Array[Array[(A, A)]])(implicit n: Numeric[A]): Array[(A, A)] = {
    val list = for {
      i <- matr.indices
      j <- matr(i).indices
      if NashOptimal(matr, i, j)
    } yield matr(i)(j)
    list.toArray
  }

  def ParetoOptimal[A](matr: Array[Array[(A, A)]], el: (A, A))(implicit n: Numeric[A]): Boolean = {
    import n._
    for {
      row <- matr
      cel <- row
      if (cel._1 >= el._1 && cel._2 > el._2) || (cel._1 > el._1 && cel._2 >= el._2)
    } return false
    true
  }

  def getPareto[A](matr: Array[Array[(A, A)]])(implicit n: Numeric[A]): Array[(A, A)] = {
    val list = for {
      raw <- matr
      el <- raw
      if ParetoOptimal(matr, el)
    } yield el
    list
  }

  val matr = Array.tabulate(10)(_ =>
    Array.tabulate(10)(_ =>
      (Random.nextInt(100), Random.nextInt(100))
    )
  )

  matr.printMe()
  println("Nash: ")
  getNash(matr).printMe()
  println("Pareto: ")
  getPareto(matr).printMe()
  println("Intersection: ")
  getNash(matr).intersect(getPareto(matr)).printMe()

}
