package Lab3

import Util.ArrayOps.ArrayOps
import breeze.linalg._

object Task2 extends App {
  val matr = Array(Array((3, 1), (5, 0)), Array((9, 6), (2, 3))) // 3 var
  //val matr = Array(Array((0, 1), (9, 2)), Array((4, 6), (6, 3)))
  val A = matr.map(_.map(_._1.toDouble))
  val B = matr.map(_.map(_._2.toDouble))
  val v = DenseMatrix((1.0, 1.0))

  val Am = new DenseMatrix(2, 2, A.flatten).t
  val Bm = new DenseMatrix(2, 2, B.flatten).t

  val nash = Task1.getNash(matr)
  println("Nash: ")
  nash.printMe()

  val v1 = (1.0 / (v * inv(Am).toDenseMatrix * v.t)).data(0)
  val v2 = (1.0 / (v * inv(Bm).toDenseMatrix * v.t)).data(0)
  println("v1 = " + v1)
  println("v2 = " + v2)

  val x = v2 * (v * inv(Bm).toDenseMatrix)
  val y = v1 * (inv(Am).toDenseMatrix * v.t)

  println("X = ")
  x.data.printMe()
  println("Y = ")
  y.data.printMe()

}
