import Util.ArrayOps.ArrayOps

val matr = Array(
  Array(-0.0637, 0.0242, 0.102),
  Array(0.0955, 0.0637, -0.153),
  Array(-0.00318, -0.0688, 0.105)
)

val matrtr = matr.transpose

val k = matrtr.map(_.sum).sum

print("Price = " + 1.0 / k)
print("x = ")
matrtr.map(_.sum / k).printMe()
print("y = ")
matr.map(_.sum / k).printMe()

9.857122686797567 - 1.0 / k