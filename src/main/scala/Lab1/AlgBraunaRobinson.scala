package Lab1

import Util.ArrayOps.ArrayOps
import de.sciss.chart.api._

case class AlgBraunaRobinson(private val matrix: Array[Array[Double]]) {
  matrix.foreach(x => assert(matrix.length == x.length))

  private val dim = matrix.length
  private val trans = matrix.transpose

  def start(shadow: Boolean = false): (Array[Double], Array[Double]) = {
    var priceA: Array[Double] = Array.tabulate(dim)(_ => 0)
    var priceB: Array[Double] = Array.tabulate(dim)(_ => 0)

    var Ak: List[Double] = Nil
    var Bk: List[Double] = Nil

    var pickA: List[Int] = 0 :: Nil
    var pickB: List[Int] = 0 :: Nil

    var errors: List[(Int, Double)] = Nil
    var Ae: List[(Int, Double)] = Nil
    var Be: List[(Int, Double)] = Nil

    @scala.annotation.tailrec
    def iterate(k: Int): Unit = {
      if (!shadow) println(s"Iterate $k:")

      priceA = priceA.zip(trans(pickB.head)).map { case (x, y) => x + y }
      if (!shadow) println("A:")
      if (!shadow) priceA.printMe()

      priceB = priceB.zip(matrix(pickA.head)).map { case (x, y) => x + y }
      if (!shadow) println("B:")
      if (!shadow) priceB.printMe()

      Ak = priceA.max / k :: Ak
      Bk = priceB.min / k :: Bk

      val e = Ak.min - Bk.max
      errors = (k, e) :: errors
      Ae = (k, Ak.min) :: Ae
      Be = (k, Bk.max) :: Be

      if (!shadow) println(s"ak = ${Ak.head}, bk = ${Bk.head}, e = $e")
      if (e > 0.1) {
        pickA = priceA.zipWithIndex.maxBy(_._1)._2 :: pickA
        pickB = priceB.zipWithIndex.minBy(_._1)._2 :: pickB
        iterate(k + 1)
      }
    }

    iterate(1)

    if (!shadow) {
      println("Result!")
      println(s"Ak.min = ${Ak.min} - Bk.max = ${Bk.max}")
      println(s"Price = ${(Ak.min + Bk.max) / 2}")

      println("strategy A:")
      strategyFromChoices(pickA).printMe()

      println("strategy B:")
      strategyFromChoices(pickB).printMe()

      val chart = XYLineChart(errors)
      chart.title = "Error/n"
      chart.show()

      val data = List(
        ("Gamer A", Ae),
        ("Gamer B", Be)
      )
      val chart2 = XYLineChart(data)
      chart2.title = "Game price/n"
      chart2.show()
    }

    (strategyFromChoices(pickA), strategyFromChoices(pickB))
  }

  private def strategyFromChoices(array: List[Int]) =
    Array.tabulate(dim)(x => array.count(_ == x).toDouble / array.length)
}